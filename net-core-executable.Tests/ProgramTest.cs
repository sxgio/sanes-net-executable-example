using Microsoft.VisualStudio.TestTools.UnitTesting;
using net_core_executable;

namespace net_core_executable.Test
{
    [TestClass]
    public class ProgramTest
    {

        [TestMethod]
        public void TestMainKnown()
        {
                var name = "named";
                Program.Main(new string[]{name});
                Assert.AreEqual(Program.GetName(), name);
        }

        [TestMethod]
        public void TestMainUnknown()
        {
                Program.Main(new string[]{"one","two"});
                Assert.AreEqual(Program.GetName(), "unknown");
                Program.Main(new string[]{});
                Assert.AreEqual(Program.GetName(), "unknown");
        }
    }
}

