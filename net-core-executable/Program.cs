﻿using System;

namespace net_core_executable
{
    // Main entry point
    public static class Program
    {
        // Static name property
        private static string name;

        // Static Getter. Don't do that in real code. 
        public static string GetName() {
            return name;
        }

        // Simple entry point for testing purposes
        public static void Main(string[] args)
        {
            // Check value received contains some data
            if (args != null &&  args.Length == 1) {
                name = args[0];
            } else {
                name = "unknown";
            }
            // For Debugging purposes. 
            Console.WriteLine("Hello "+ name + "!");
        }
    }
}

