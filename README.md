# Net-Executable Example Project

Example of a POC project to show how to make an executable using the dotnet core command.

## Introduction

Project created using the dotnet core command. It provides an easy-to-use guide to test the code and push both the testing results to the sonar server and an executable to the nexus server

[![dotnet version](https://img.shields.io/badge/dotnet%20version-2.2.108-brightgreen.svg?style=flat&logo=visual-studio-code)](https://dotnet.microsoft.com/download)
[![coverage](https://img.shields.io/badge/coverage%20-100%25-brightgreen.svg?style=flat&logo=test)](https://sanes-qa-sonar-san-alm-st-dev.appls.san01.san.dev.bo1.paas.cloudcenter.corp/dashboard?id=SANES-NET-EXECUTABLE)
[![Git Repository](https://img.shields.io/badge/repository-url-green?style=flat&logo=git)](https://github.alm.europe.cloudcenter.corp/sanes-alm-darwin/net-core-executable-example.git)

## Guidelines

Please, follow the commands below:

- Scaffolding:

```{.sh}
mkdir net-core-executable && cd net-core-executable
dotnet new console
```

- Running to check everything was configured properly:

```{.sh}
dotnet run
```

- Modify the net-core-executable.csproj file to something like:

```{.xml}
<Project Sdk="Microsoft.NET.Sdk">
  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>netcoreapp2.2</TargetFramework>
    <RootNamespace>net_core_executable</RootNamespace>
    <IsPackable>true</IsPackable>
    <Version>0.0.1-SNAPSHOT</Version>
    <Authors>Dev Lead, Developers...</Authors>
    <Company>Santander Tecnología</Company>
    <GeneratePackageOnBuild>true</GeneratePackageOnBuild>
    <GenerateProgramFile>false</GenerateProgramFile>
    <IsTestProject>false</IsTestProject>
   <ProjectGuid>{E2CEBBAF-6DF7-41E9-815D-9AD4CF90C844}</ProjectGuid>
  </PropertyGroup>
</Project>
```

- Modify the Program.cs file to something like:

```{.cs}
namespace net_core_executable
{
    // Main entry point
    public static class Program
    {
        // Static name property
        private static string name;

        // Static Getter. Don't do that in real code.
        public static string GetName() {
            return name;
        }

        // Simple entry point for testing purposes
        public static void Main(string[] args)
        {
            // Check value received contains some data
            if (args != null &&  args.Length == 1) {
                name = args[0];
            } else {
                name = "unknown";
            }
            // For Debugging purposes.
            Console.WriteLine("Hello "+ name + "!");
        }
    }
}
```

- Scaffolding the testing project (MSTest. NUnit and xUnit also provided but not explained here):

```{.sh}
cd .. && mkdir net-core-executable.Tests && cd net-core-executable.Tests
dotnet new mstest -n net-core-executable.Tests
dotnet add reference ../net-core-executable/net-core-executable.csproj
```

- Create a new file named as ProgramTest.cs that should look like:

```{.cs}
using Microsoft.VisualStudio.TestTools.UnitTesting;
using net_core_executable;

namespace net_core_executable.Test
{
    [TestClass]
    public class ProgramTest
    {

        [TestMethod]
        public void TestMainKnown()
        {
                var name = "named";
                Program.Main(new string[]{name});
                Assert.AreEqual(Program.GetName(), name);
        }

        [TestMethod]
        public void TestMainUnknown()
        {
                Program.Main(new string[]{"one","two"});
                Assert.AreEqual(Program.GetName(), "unknown");
                Program.Main(new string[]{});
                Assert.AreEqual(Program.GetName(), "unknown");
        }
    }
}
```

- Just in case coverlet is not available in the system

```{.sh}
dotnet add package coverlet.msbuild
```

- Modify the net-core-executable.Tests.csproj to something like:

```{.xml}
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <TargetFramework>netcoreapp2.2</TargetFramework>
    <RootNamespace>net_core_executable.Tests</RootNamespace>

    <IsPackable>false</IsPackable>
  </PropertyGroup>

  <ItemGroup>
    <PackageReference Include="coverlet.msbuild" Version="2.6.3">
      <IncludeAssets>runtime; build; native; contentfiles; analyzers</IncludeAssets>
      <PrivateAssets>all</PrivateAssets>
    </PackageReference>
    <PackageReference Include="Microsoft.NET.Test.Sdk" Version="15.9.0" />
    <PackageReference Include="MSTest.TestAdapter" Version="1.3.2" />
    <PackageReference Include="MSTest.TestFramework" Version="1.3.2" />
  </ItemGroup>

  <ItemGroup>
    <ProjectReference Include="..\net-core-executable\net-core-executable.csproj" />
  </ItemGroup>

</Project>
```

- Global access to donet-sonarscanner

```{.sh}
dotnet tool install --global dotnet-sonarscanner
```

- Access to the project and execute the sonarscanner command

```{.sh}
cd ../net-core-executable
dotnet sonarscanner begin /k:"SANES-NET-EXECUTABLE" /n:"SANES-NET-EXECUTABLE" /v:"0.0.1-SNAPSHOT" /d:sonar.host.url="http://sonar-server:9000" /d:sonar.language="cs" /d:sonar.exclusions="**/bin/**/*,**/obj/**/*" /d:sonar.cs.opencover.reportsPaths="../net-core-executable.Tests/lcov.opencover.xml"
dotnet build
dotnet test ../net-core-executable.Tests/net-core-executable.Tests.csproj /p:CollectCoverage=true /p:CoverletOutputFormat=\"opencover,lcov\" /p:CoverletOutput=lcov
dotnet sonarscanner end
```

- Use either *dotnet* or *curl command* to push the package to the *Nexus* server:

  - Update the right content to the nexus server using dotnet push command:

  ```{.sh}
  dotnet nuget push bin/Debug/net-core-executable.0.0.1-SNAPSHOT.nupkg -k [API-KEY] -s http://nexus-server/repository/nuget-hosted/net-core-executable.0.0.1-SNAPSHOT
  ```

  - Update the right content to the nexus server using the curl command:

  ```{.sh}
  curl -u nexus-user:nexus-password -X PUT -v -include -F package=@net-core-executable.0.0.1-SNAPSHOT.nupkg http://nexus-server/repository/nuget-hosted/
  ```

## Where to see results

### Nexus Server

[QA Nexus Server](https://sanes-qa-nexus-san-alm-st-dev.appls.san01.san.dev.bo1.paas.cloudcenter.corp/repository/nuget-group/net-core-executable/0.0.1-SNAPSHOT)

### Sonar Server

[Sonar Server ALM Dev](https://sanes-qa-sonar-san-alm-st-dev.appls.san01.san.dev.bo1.paas.cloudcenter.corp/dashboard?id=SANES-NET-EXECUTABLE)

## Contributors

**Sergio Martín** <sergio.martin@sxgio.net>

## Contributing

[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.alm.europe.cloudcenter.corp/sanes-alm-darwin/net-core-executable-example/blob/master/CONTRIBUTING.md)

## License

© 2019. Santander Tecnología.
