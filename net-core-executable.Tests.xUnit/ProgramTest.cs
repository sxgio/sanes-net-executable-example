using System;
using Xunit;
using net_core_executable;

namespace net_core_executable.Tests.xUnit
{
    public class ProgramTest
    {
        [Fact]
        public void TestMainKnown()
        {
            var name = "named";
            Program.Main(new string[]{name});
            Assert.Equal(Program.GetName(), name);
        }

        [Fact]
        public void TestMainUnknown()
        {
            Program.Main(new string[]{"one","two"});
            Assert.Equal(Program.GetName(), "unknown");
            Program.Main(new string[]{});
            Assert.Equal(Program.GetName(), "unknown");
        }
    }
}