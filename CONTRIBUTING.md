# Contributing to the net-core-executable-example project

First of all, thank you for taking the time to contribute! 👍

The following project is a set of guidelines for contributing to to SANES-ALM and SANES-QA and their ecosystems.
These are mostly guidelines, not strict rules. Use your best judgment, and feel free to propose changes to this document via pull requests. This document was inspired by the awesome Atom contribution guidelines.

## Table of contents

[I don't want to read this whole thing, I just have a question!!!](#i-dont-want-to-read-this-whole-thing-i-just-have-a-question)

[How Can I Contribute?](#how-can-i-contribute)

* [Your First Code Contribution](#your-first-code-contribution)
* [GitFlow](#gitflow)
* [Commit message convention](#commit-message-convention)

## I don't want to read this whole thing, I just have a question

Note: Please don't file an issue to ask a question. You'll get faster results by using the following slack channel:

We have an official **Microsoft Teams** channel where core maintainers can answer your questions in a more direct fashion. We encourage you to join the channel  [**General**](https://teams.microsoft.com/l/channel/19%3a06de8fef18364f3185e9a889f84115ce%40thread.skype/General?groupId=6fd6e95a-3db7-4d93-bcad-cb8b5eb1be2e&tenantId=35595a02-4d6d-44ac-99e1-f9ab4cd872db) on **ALM-DARWING**

## How Can I Contribute

### Your First Code Contribution

Are you still unsure how you may contribute to the SOP ecosystem? You may start by looking through open issues or your own feature requests (if any). We are strong supporters of the inner source philosophy and we will help you as much as we can to add your code to the repository. If you are unsure about the way to do it, just [ask](#i-dont-want-to-read-this-whole-thing-i-just-have-a-question).

#### GitFlow

* Clone the main project with either one of the following commands:

```{.sh}
git clone git@github.alm.europe.cloudcenter.corp:sanes-alm-darwin/net-core-executable-example.git
git clone https://github.alm.europe.cloudcenter.corp/sanes-alm-darwin/net-core-executable-example.git
```

* Fork the project from GitHub UI

* Add a remote in your git:

```{.sh}
git remote add personalhttps://github.alm.europe.cloudcenter.corp/x311644/net-core-executable-example.git
```

* You should have something like:

```{.sh}
$ git remote -v
origin  https://github.alm.europe.cloudcenter.corp/sanes-alm-darwin/net-core-executable-example.git (fetch)
origin  https://github.alm.europe.cloudcenter.corp/sanes-alm-darwin/net-core-executable-example.git (push)
personal https://github.alm.europe.cloudcenter.corp/x311644/net-core-executable-example.git (fetch)
personal https://github.alm.europe.cloudcenter.corp/x311644/net-core-executable-example.git (push)
```

* create your branch. For intance:

```{.sh}
git fetch origin
git rebase origin/master
git checkout -b 'GITHUB-#1-net-executable-test'
```

( 2 first commands aims to ensure you are up to date with the master before creating a branch.)

* Add and commit your change

```{.sh}
git add .; git commit -m "doc: add package.json"
```

other change needs to be added..

```{.sh}
git add .: git commit -m "doc: add contributing.md"
```

* Rebase before to push your change and push a merge request.

```{.sh}
git rebase -i $(git merge-base origin GITHUB-#1-net-executable-test)
```

and rewrite `pick` by squash for all extra commit so the first one must remain with the word `pick`.

For example:

```{.sh}
pick dp2ed3 doc: add package.json
squash 98dawj doc: add section local development
squash 098qdw doc: add commit convention
```

* In the step of rebase, take the opportunity to write a well documented commit.

For instance:

```{.txt}
fix(middleware): ensure Range headers adhere more closely to RFC 2616

Add one new dependency, use `range-parser` (Express dependency) to compute
range. It is more well-tested in the wild.

Fixes #2310
```

* Then you are ready to push your code.

```{.sh}
git fetch origin
git rebase origin/master
git push -f personal GITHUB-#1-net-executable-test
```

As a result you will be pointed out to the pull request you should create:

```{.txt}
Enumerando objetos: 7, listo.
Contando objetos: 100% (7/7), listo.
Compresión delta usando hasta 4 hilos
Comprimiendo objetos: 100% (5/5), listo.
Escribiendo objetos: 100% (6/6), 784 bytes | 41.00 KiB/s, listo.
Total 6 (delta 2), reusado 0 (delta 0)
remote: Resolving deltas: 100% (2/2), completed with 1 local object.
remote:
remote: Create a pull request for 'GITHUB-#1-net-executable-test' on GitHub by visiting:y visiting:                                                                  posantander-es/net-core-executable-example/pull/new/GITHUB-#1-net-executable-test
remote:      https://github.alm.europe.cloudcenter.corp/x311644/net-core-executable-example/pull/new/GITHUB-#1-net-executable-tester
remote:
To https://github.alm.europe.cloudcenter.corp/x311644/net-core-executable-example.git
 * [new branch]      GITHUB-#1-net-executable-test -> GITHUB-#1-net-executable-test
```

(The 2 first commands are there to sync with origin and fix potential conflicts)

* Have a look in your fork in Gitlab.

In my case, here:
[Local Repo](https://github.alm.europe.cloudcenter.corp/x311644/net-core-executable-example.git)

Now, you are ready to create the pull request (follow the github indications):
`From x311644/importer:GITHUB-#1-net-executable-test into SANES-ALM/net-core-executable-example/importer:master`

#### Commit message convention

We are following the standard message convention of git. Go [here](http://karma-runner.github.io/2.0/dev/git-commit-msg.html) to dig into all the specifications.
