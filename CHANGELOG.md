# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.1.2]

## 2019-11-18

## Changed

- README.md. Clean up markdown style and badges added.
- CHANGELOG.md updated

## Added

- Tests in xUnit created using dotnet new
- Tests in NUnit created using dotnet new

## [0.1.1]

## 2019-09-21

## Added

- net-executable project

- net-executable.Test project

## [0.1.0]

## 2019-09-17

## Added

- Generic Scaffolding

## [Unreleased]

## 2019-11-06

## Added

- Tests in xUnit created using dotnet new
- Tests in NUnit created using dotnet new

## 2019-10-23

## Changed

- README.md and CONTRIBUTING.md refractorized. Clean up markdown style

## 2019-09-21

## Added

- net-executable project

- net-executable.Test project

## 2019-09-17

## Added

- Generic Scaffolding
